module ~/projects/gopdf

go 1.14

require (
	github.com/SebastiaanKlippert/go-wkhtmltopdf v1.5.0
	github.com/gin-gonic/gin v1.6.3
)
