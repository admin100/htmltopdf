# HtmlToPDF
A simple html to pdf server written in go.

## Installation

Precompiled binaries for MacOS, linux and Windows can be found [here](https://gitlab.com/admin100/htmltopdf/-/jobs/artifacts/master/browse?job=build-binaries) 

### Windows

1. Build from source or download precompiled windows executable [htmltopdf.exe](https://gitlab.com/admin100/htmltopdf/-/jobs/artifacts/master/browse?job=build-binaries) 

2. Download and install [wkhtmltopdf](https://wkhtmltopdf.org/downloads.html) for windows.

3. Define the "WKHTMLTOPDF_PATH" variable
   ```cmd
   set WKHTMLTOPDF_PATH=C:/Program Files/wkhtmltopdf/bin/wkhtmltopdf
   ```
   To avoid repeating the above process all the time, you can set the variable permanently:

   ![alt text](./docs/images/windows_variable.png "Logo Title Text 1")

4. Run the executable in powershell
    ```shell
    ./htmltopdf
    ```
    You can add the binary to the system path if you wish

### Linux

1. Build from source or download precompiled linux binary [htmltopdf](https://gitlab.com/admin100/htmltopdf/-/jobs/artifacts/master/browse?job=build-binaries) 

2. Download and install [wkhtmltopdf](https://wkhtmltopdf.org/downloads.html) for linux.

3. Define the "WKHTMLTOPDF_PATH" variable
   ```cmd
   export WKHTMLTOPDF_PATH=/usr/local/bin/wkhtmltopdf

4. Run the binary
    ```shell
    ./htmltopdf
    ```
    You can add the binary to the system path if you wish

### MacOS
1. Build from source or download precompiled macos binary [htmltopdf.darwin.amd64](https://gitlab.com/admin100/htmltopdf/-/jobs/artifacts/master/browse?job=build-binaries) 
2. Download and install [wkhtmltopdf](https://wkhtmltopdf.org/downloads.html) for Mac.

3. Define the "WKHTMLTOPDF_PATH" variable
   ```cmd
   export WKHTMLTOPDF_PATH=/path/to/binary

4. Run the binary
    ```shell
    ./htmltopdf.darwin.amd64
    ```
    You can add the binary to the system path if you wish

### Docker

```
docker run -p 8089:8089 registry.gitlab.com/admin100/htmltopdf
```

Or with docker compose if you prefer
```yaml
version: "3.4"
services:
    htmltopdf:
        image: "registry.gitlab.com/admin100/htmltopdf"
        ports:
            - 8089:8089
```

## Usage
Once the server running you can visit https://localhost:8089/ in your browser to see if it is running.

Post your html string to the server and obtain a base64 encoded pdf file in return.

```shell
curl --location --request POST 'http://localhost:8089/' \
--header 'Content-Type: application/json' \
--data-raw '{
	"html": "<h1>Hello World</h1>"
}'
```

response payload
```json
{
    "pdf": "base64encodedpdf"
}
```

## Examples

### Python
```python
import base64
import json
import requests

def html_to_pdf():
    html = "<h1>Hello World</h1>"
    headers = {
        'Content-Type': 'application/json'
    }
    payload = json.dumps({
        "html": html
    })
    response = requests.request("POST", "http://localhost:8089/", headers=headers, data=payload)

    data = base64.b64decode(response.json()["pdf"])

    f = open('./hello_world.pdf', 'wb')
    f.write(data)
    f.close()
```

## Build / Contributing

```go
go run htmltopdf.go //run build
go build htmltopdf.go // compile from source
```

### Docker

```shell
docker build -t registry.gitlab.com/admin100/htmltopdf .
docker push registry.gitlab.com/admin100/htmltopdf
```


