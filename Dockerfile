FROM golang:1.14-buster

RUN wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.buster_amd64.deb
RUN apt -y update && apt -y install ./wkhtmltox_0.12.5-1.buster_amd64.deb && rm wkhtmltox_0.12.5-1.buster_amd64.deb
ENV WKHTMLTOPDF_PATH /usr/local/bin/wkhtmltopdf
RUN mkdir /app
WORKDIR /app

COPY . .
RUN go build ./htmltopdf.go

CMD ["./htmltopdf"]
