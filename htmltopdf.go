package main

import (
	"bytes"
	"net/http"
	"os"

	"github.com/SebastiaanKlippert/go-wkhtmltopdf"
	"github.com/gin-gonic/gin"
)

/*
Path to the wkhtmltopdf binaries/executables:
Windows example: "C:/Program Files/wkhtmltopdf/bin/wkhtmltopdf"
Debian: "/usr/local/bin/wkhtmltopdf"
*/

var path string = os.Getenv("WKHTMLTOPDF_PATH")

type Options struct {
	ORIENTATION string `form:"orientation" json:"orientation" xml:"orientation"`
}

// RequestPayload : type struct for incoming RequestPayload
type RequestPayload struct {
	HTML    string  `form:"html" json:"html" xml:"html"  binding:"required"`
	OPTIONS Options `form:"options" json:"options" xml:"options"`
}

// Convert : convert the html string to pdf bytes
func Convert(html string, options Options) ([]byte, error) {

	wkhtmltopdf.SetPath(path)

	page := wkhtmltopdf.NewPageReader(bytes.NewReader([]byte(html)))
	page.DisableExternalLinks.Set(false)

	pdfg, err := wkhtmltopdf.NewPDFGenerator()

	pdfg.AddPage(page)

	pdfg.MarginBottom.Set(0)
	pdfg.MarginTop.Set(0)
	pdfg.MarginLeft.Set(0)
	pdfg.MarginRight.Set(0)
	pdfg.Orientation.Set(options.ORIENTATION)
	pdfg.Dpi.Set(600)

	pdfg.Create()

	return pdfg.Bytes(), err

}

func getRoot(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{})
}

func postRoot(c *gin.Context) {

	var json RequestPayload

	if err := c.ShouldBindJSON(&json); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	pdf, err := Convert(json.HTML, json.OPTIONS)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"pdf": pdf})

}

func main() {
	router := gin.Default()
	router.POST("/", postRoot)
	router.GET("/", getRoot)
	router.Run(":8089")
}
